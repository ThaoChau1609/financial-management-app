package com.example.financialmanagement.repository;

import android.content.Context;

import com.example.financialmanagement.App;
import com.example.financialmanagement.AppExecutors;
import com.example.financialmanagement.repository.home.HomeRepository;
import com.example.financialmanagement.repository.login.UserReponsitory;
import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;

public class InjectorUtils {
    public static HomeRepository provideHomeRepository(Context context) {
        AppExecutors executors = AppExecutors.getInstance();

        return new HomeRepository(executors);
    }

    public static UserReponsitory provideUserReponsitory(Context context) {
        AppExecutors executors = AppExecutors.getInstance();
        return new UserReponsitory(executors, context);
    }
    public static MoneyJarRepository provideMoneyJarRepository(Context context){
        AppExecutors executors = AppExecutors.getInstance();
        return new MoneyJarRepository(executors, context);
    }
}
