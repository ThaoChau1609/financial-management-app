package com.example.financialmanagement.view;

import android.content.Context;

import com.example.financialmanagement.repository.InjectorUtils;
import com.example.financialmanagement.repository.home.HomeRepository;
import com.example.financialmanagement.repository.login.UserReponsitory;
import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;
import com.example.financialmanagement.view.exchange.viewmodel.ExchangeActivityViewModelFactory;
import com.example.financialmanagement.view.exchange.viewmodel.ListJarActivityViewModelFactory;
import com.example.financialmanagement.view.home.fragments.history.viewmodel.HistoryFragmentViewModelFactory;
import com.example.financialmanagement.view.home.fragments.home.viewmodel.HomeFragmentViewModelFactory;
import com.example.financialmanagement.view.home.fragments.menu.viewmodel.MenuHomeFragmentViewModelFactory;
import com.example.financialmanagement.view.home.viewmodel.MainActivityViewModelFactory;
import com.example.financialmanagement.view.login.viewmodel.LoginViewModelFactory;
import com.example.financialmanagement.view.thietlaphu.viewmodel.ThietLapHuViewModelFactory;

public class ViewModelProviderUtils {
    public static MainActivityViewModelFactory provideMainActivityViewModelFactory(Context context) {
        HomeRepository homeRepository = InjectorUtils.provideHomeRepository(context);
        UserReponsitory userReponsitory = InjectorUtils.provideUserReponsitory(context);
        return new MainActivityViewModelFactory(homeRepository, userReponsitory);
    }

    public static LoginViewModelFactory provideLoginViewModelFactory(Context context) {
        UserReponsitory userReponsitory = InjectorUtils.provideUserReponsitory(context);
        return new LoginViewModelFactory(userReponsitory);
    }

    public static MenuHomeFragmentViewModelFactory provideMenuHomeFragmentViewModelFactory(Context context) {
        UserReponsitory userReponsitory = InjectorUtils.provideUserReponsitory(context);
        return new MenuHomeFragmentViewModelFactory(userReponsitory);
    }

    public static ThietLapHuViewModelFactory provideThietLapHuViewModelFactory(Context context) {
        MoneyJarRepository moneyJarRepository = InjectorUtils.provideMoneyJarRepository(context);
        return new ThietLapHuViewModelFactory(moneyJarRepository);
    }
    public static HomeFragmentViewModelFactory provideHomeFragmentViewModelFactory(Context context){
        MoneyJarRepository moneyJarRepository = InjectorUtils.provideMoneyJarRepository(context);
        return new HomeFragmentViewModelFactory(moneyJarRepository);
    }

    public static ExchangeActivityViewModelFactory provideExchangeActivityViewModelFactory(Context context) {
        MoneyJarRepository moneyJarRepository = InjectorUtils.provideMoneyJarRepository(context);
        return new ExchangeActivityViewModelFactory(moneyJarRepository);
    }

    public static ListJarActivityViewModelFactory provideListJarActivityViewModelFactory(Context context) {
        MoneyJarRepository moneyJarRepository = InjectorUtils.provideMoneyJarRepository(context);
        return new ListJarActivityViewModelFactory(moneyJarRepository);
    }
    public static HistoryFragmentViewModelFactory provideHistoryFragmentViewModelFactory(Context context){
        MoneyJarRepository moneyJarRepository = InjectorUtils.provideMoneyJarRepository(context);
        return new HistoryFragmentViewModelFactory(moneyJarRepository);
    }
}
