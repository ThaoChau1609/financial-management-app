package com.example.financialmanagement.view.home.fragments.menu;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.financialmanagement.R;
import com.example.financialmanagement.entities.User;
import com.example.financialmanagement.view.ViewModelProviderUtils;
import com.example.financialmanagement.view.home.fragments.menu.viewmodel.MenuHomeFragmentViewModel;
import com.example.financialmanagement.view.home.fragments.menu.viewmodel.MenuHomeFragmentViewModelFactory;
import com.example.financialmanagement.view.thietlaphu.ThietLapHuActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuHomeFragment extends Fragment implements View.OnClickListener {


    // User Information
    private ImageButton btnAvatar;
    private TextView tvUserName;
    // Menu
    private RelativeLayout itemThietLapHu;
    private RelativeLayout itemGiaodichDichky;
    private RelativeLayout itemLapKehoach;

    private MenuHomeFragmentViewModel viewModel;

    public MenuHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MenuHomeFragmentViewModelFactory factory = ViewModelProviderUtils.provideMenuHomeFragmentViewModelFactory(getContext());
        viewModel = ViewModelProviders.of(this, factory).get(MenuHomeFragmentViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addControls(view);
        addEvents();

        viewModel.getUserInformation();
        viewModel.getUserInfomationLiveData().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if (user != null) {
                    tvUserName.setText(user.getName());
                    if (getActivity() != null) {
                        Glide.with(getActivity()).load(user.getImage()).into(btnAvatar);
                    }
                }
            }
        });
    }

    private void addEvents() {
        btnAvatar.setOnClickListener(this);
        itemThietLapHu.setOnClickListener(this);
        itemGiaodichDichky.setOnClickListener(this);
        itemLapKehoach.setOnClickListener(this);
    }

    private void addControls(View view) {
        btnAvatar = view.findViewById(R.id.img_avater_menu);
        tvUserName = view.findViewById(R.id.tv_username);
        itemThietLapHu = view.findViewById(R.id.main_activity_item_thietlap_hu);
        itemGiaodichDichky = view.findViewById(R.id.main_activity_item_giaodich_dinhky);
        itemLapKehoach = view.findViewById(R.id.main_activity_item_lap_kehoach);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnAvatar)) {
            // click avatar

        }

        if (v.equals(itemThietLapHu)) {
            // click menu thiet lap hu
            Intent intent = new Intent(getActivity(), ThietLapHuActivity.class);
            startActivity(intent);
        }
        if (v.equals(itemGiaodichDichky)) {
            // click menu giao dich dinh ky

        }
        if (v.equals(itemLapKehoach)) {
            // click menu lap ke hoach

        }
    }
}
