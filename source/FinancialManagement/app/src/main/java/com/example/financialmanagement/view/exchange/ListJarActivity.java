package com.example.financialmanagement.view.exchange;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.financialmanagement.R;
import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.view.ViewModelProviderUtils;
import com.example.financialmanagement.view.exchange.viewmodel.ListJarActivityViewModel;
import com.example.financialmanagement.view.exchange.viewmodel.ListJarActivityViewModelFactory;

import java.text.DecimalFormat;
import java.util.List;

public class ListJarActivity extends AppCompatActivity {

    public static final String KEY_MONEY_JAR_SELECTED = "KEY_MONEY_JAR_SELECTED";
    public static final String KEY_MONEY_JARALL_SELECTED = "KEY_MONEY_JARALL_SELECTED";

    private int currentMoneyJarIC;
    private int[] moneyJarImageList = new int[] {
            R.drawable.ic_jar1,
            R.drawable.ic_jar2,
            R.drawable.ic_jar3,
            R.drawable.ic_jar4,
            R.drawable.ic_jar5,
            R.drawable.ic_jar6
    };

    private LinearLayout layoutJarList;

    private RelativeLayout relativeJarAll;
    private ListJarActivityViewModel viewModel;
    private TextView tvJarAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_jar);
        layoutJarList = findViewById(R.id.list_jar_layout);
        relativeJarAll = findViewById(R.id.relativeLayout_jarAll);
        tvJarAll = findViewById(R.id.view_itemJarAll_tvname);
        relativeJarAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                String tvJarAllName = tvJarAll.getText().toString();
                intent.putExtra(KEY_MONEY_JARALL_SELECTED, tvJarAllName);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        ListJarActivityViewModelFactory factory = ViewModelProviderUtils.provideListJarActivityViewModelFactory(this);
        viewModel = ViewModelProviders.of(this, factory).get(ListJarActivityViewModel.class);

        viewModel.getMoneyJarUserDetailList();
        viewModel.getMoneyJarUserDetailListliveData().observe(this, new Observer<List<MoneyJarUserDetail>>() {
            @Override
            public void onChanged(List<MoneyJarUserDetail> moneyJarUserDetails) {
                if (moneyJarUserDetails != null) {
                    currentMoneyJarIC = 0;
                    layoutJarList.removeAllViews();
                    for (MoneyJarUserDetail moneyJarUserDetail : moneyJarUserDetails) {
                        layoutJarList.addView(createItemMoneyJar(moneyJarUserDetail));
                    }
                }
            }
        });
    }

    private View createItemMoneyJar(final MoneyJarUserDetail moneyJar) {
        View view = getLayoutInflater().inflate(R.layout.view_itemjar_dashboard, null);

        TextView tvJarName = view.findViewById(R.id.view_itemjar_dashboard_tvJarName);
        ImageView imgJar = view.findViewById(R.id.view_itemjar_dashboard_imgJar);
        TextView tvBudget = view.findViewById(R.id.view_itemjar_dashboard_tvBudget);
        // set Data
        tvJarName.setText(moneyJar.getMoneyJar().getName());
        tvBudget.setText(formatVNcurrency(String.valueOf(moneyJar.getBudget())));
        if (URLUtil.isValidUrl(moneyJar.getMoneyJar().getImage())) {
            Glide.with(this).load(moneyJar.getMoneyJar().getImage()).into(imgJar);
        } else {
            if (this.currentMoneyJarIC < moneyJarImageList.length) {
                imgJar.setImageResource(moneyJarImageList[this.currentMoneyJarIC]);
                this.currentMoneyJarIC++;
            }
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                intent.putExtra(KEY_MONEY_JAR_SELECTED, moneyJar);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        return view;
    }

    private String formatVNcurrency(String price){
        double priceCurrency = Double.parseDouble(price);
        DecimalFormat formatter = new DecimalFormat("###,###,##0");
        return formatter.format(priceCurrency);
    }
}
