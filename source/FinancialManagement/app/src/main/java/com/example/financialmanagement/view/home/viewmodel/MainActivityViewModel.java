package com.example.financialmanagement.view.home.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.financialmanagement.repository.home.HomeRepository;
import com.example.financialmanagement.repository.login.UserReponsitory;

public class MainActivityViewModel extends ViewModel {

    private MutableLiveData<Boolean> loginStatusResult = new MutableLiveData<>();

    private HomeRepository homeRepository;
    private UserReponsitory userReponsitory;

    public MainActivityViewModel(HomeRepository homeRepository, UserReponsitory userReponsitory) {
        this.homeRepository = homeRepository;
        this.userReponsitory = userReponsitory;
    }

    public void checkStatusLogin() {
        this.userReponsitory.checkLoginStatus(loginStatusResult);
    }

    public MutableLiveData<Boolean> getLoginStatusResult() {
        return loginStatusResult;
    }
}
