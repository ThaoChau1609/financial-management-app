package com.example.financialmanagement.view.home.fragments.home.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;

public class HomeFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private MoneyJarRepository moneyJarRepository;

    public HomeFragmentViewModelFactory(MoneyJarRepository moneyJarRepository) {
        this.moneyJarRepository = moneyJarRepository;
    }
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new HomeFragmentViewModel(moneyJarRepository);
    }
}
