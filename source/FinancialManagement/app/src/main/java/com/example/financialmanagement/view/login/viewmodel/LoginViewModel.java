package com.example.financialmanagement.view.login.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.financialmanagement.repository.login.UserReponsitory;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<Boolean> resultLoginLiveData = new MutableLiveData<>();

    private UserReponsitory userReponsitory;

    public LoginViewModel(UserReponsitory userReponsitory) {
        this.userReponsitory = userReponsitory;
    }

    public void login(String userName, String password) {
        this.userReponsitory.login(userName, password, resultLoginLiveData);
    }

    public MutableLiveData<Boolean> getResultLoginLiveData() {
        return resultLoginLiveData;
    }
}
