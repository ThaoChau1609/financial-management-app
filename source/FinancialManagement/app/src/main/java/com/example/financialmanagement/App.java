package com.example.financialmanagement;

import android.app.Application;

import com.google.gson.Gson;

public class App extends Application {

    private static App mSelf;
    private Gson mGSon;

    @Override
    public void onCreate() {
        super.onCreate();
        mSelf = this;
        mGSon = new Gson();
    }

    public static App self() {
        return mSelf;
    }

    public Gson getGSon() {
        return mGSon;
    }
}
