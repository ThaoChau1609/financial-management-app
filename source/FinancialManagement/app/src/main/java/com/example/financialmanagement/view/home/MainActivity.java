package com.example.financialmanagement.view.home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.financialmanagement.R;
import com.example.financialmanagement.view.exchange.ExchangeActivity;
import com.example.financialmanagement.view.home.fragments.history.HistoryFragment;
import com.example.financialmanagement.view.home.fragments.home.HomeFragment;
import com.example.financialmanagement.view.home.fragments.menu.MenuHomeFragment;
import com.example.financialmanagement.view.home.fragments.report.ReportFragment;
import com.example.financialmanagement.view.home.viewmodel.MainActivityViewModel;
import com.example.financialmanagement.view.home.viewmodel.MainActivityViewModelFactory;
import com.example.financialmanagement.view.ViewModelProviderUtils;
import com.example.financialmanagement.view.login.LoginActivity;
import com.example.financialmanagement.view.thietlaphu.ThietLapHuActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity
        extends AppCompatActivity
        implements View.OnClickListener,
        BottomNavigationView.OnNavigationItemSelectedListener {

    // add new
    private ImageButton btnThemGiaodich;

    // Fragments
    private MenuHomeFragment menuHomeFragment;
    private HomeFragment homeFragment;
    private HistoryFragment historyFragment;
    private ReportFragment reportFragment;

    // bottom nav menu
    private BottomNavigationView bottomNavigationView;

    private MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init viewmodel
        MainActivityViewModelFactory factory = ViewModelProviderUtils.provideMainActivityViewModelFactory(this);
        viewModel = ViewModelProviders.of(this, factory).get(MainActivityViewModel.class);

        addControls();
        addEvents();
        addFragments();

        replaceFragment(homeFragment);

        // check status login
        viewModel.checkStatusLogin();
        viewModel.getLoginStatusResult().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean != null) {
                    if (aBoolean == false) {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(MainActivity.this, "Authentication User Suceess !", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void addFragments() {
        menuHomeFragment = new MenuHomeFragment();
        homeFragment = new HomeFragment();
        historyFragment = new HistoryFragment();
        reportFragment = new ReportFragment();
    }

    private void addEvents() {
        btnThemGiaodich.setOnClickListener(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    private void addControls() {
        btnThemGiaodich = findViewById(R.id.btn_them_giaodich);
        bottomNavigationView = findViewById(R.id.main_activity_bottomnav);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnThemGiaodich)) {
            // click them giao dich
           Intent intent = new Intent(this, ExchangeActivity.class);
           startActivity(intent);
        }
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                replaceFragment(homeFragment);
                break;
            case R.id.navigation_history:
                replaceFragment(historyFragment);
                break;
            case R.id.navigation_report:
                replaceFragment(reportFragment);
                break;
            case R.id.navigation_menu:
                replaceFragment(menuHomeFragment);
                break;
        }
        return true;
    }

    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content, fragment).commit();
    }
}
