package com.example.financialmanagement.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class MoneyJarUserDetail implements Serializable, Cloneable {
    private String id;
    private MoneyJar moneyJar;
    private User user;
    private int percent;
    private double budget;

    private Map<String, Exchange> exchangeMap;

    public MoneyJarUserDetail(String id, MoneyJar moneyJar, User user, int percent, double budget, Map<String, Exchange> exchangeMap) {
        this.id = id;
        this.moneyJar = moneyJar;
        this.user = user;
        this.percent = percent;
        this.budget = budget;
        this.exchangeMap = exchangeMap;
    }

    public MoneyJarUserDetail(MoneyJar moneyJar) {
        this.id = UUID.randomUUID().toString();
        this.moneyJar = moneyJar;
    }

    public MoneyJarUserDetail(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MoneyJar getMoneyJar() {
        return moneyJar;
    }

    public void setMoneyJar(MoneyJar moneyJar) {
        this.moneyJar = moneyJar;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public Map<String, Exchange> getExchangeList() {
        return exchangeMap;
    }

    public void setExchangeList(Map<String, Exchange> exchangeMap) {
        this.exchangeMap = exchangeMap;
    }

    public void addNewExChange(Exchange exchange) {
        if (this.exchangeMap == null) {
            this.exchangeMap = new HashMap<>();
        }
            this.exchangeMap.put(exchange.getId(), exchange);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MoneyJarUserDetail that = (MoneyJarUserDetail) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "MoneyJarUserDetail{" +
                "id='" + id + '\'' +
                ", moneyJar=" + moneyJar +
                ", user=" + user +
                ", percent=" + percent +
                ", budget=" + budget +
                '}';
    }
}
