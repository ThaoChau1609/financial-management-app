package com.example.financialmanagement.view.thietlaphu.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;

public class ThietLapHuViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private MoneyJarRepository moneyJarRepository;

    public ThietLapHuViewModelFactory(MoneyJarRepository moneyJarRepository) {
        this.moneyJarRepository = moneyJarRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ThietLapHuViewModel(moneyJarRepository);
    }
}
