package com.example.financialmanagement.view.home.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.financialmanagement.repository.home.HomeRepository;
import com.example.financialmanagement.repository.login.UserReponsitory;

public class MainActivityViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private HomeRepository homeRepository;
    private UserReponsitory userReponsitory;

    public MainActivityViewModelFactory(HomeRepository homeRepository, UserReponsitory userReponsitory) {
        this.homeRepository = homeRepository;
        this.userReponsitory = userReponsitory;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        //noinspection unchecked
        return (T) new MainActivityViewModel(this.homeRepository, this.userReponsitory);
    }
}
