package com.example.financialmanagement.view.exchange.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;

import java.util.List;

public class ListJarActivityViewModel extends ViewModel {

    private MutableLiveData<List<MoneyJarUserDetail>> moneyJarUserDetailListliveData = new MutableLiveData<>();

    private MoneyJarRepository moneyJarRepository;

    public ListJarActivityViewModel(MoneyJarRepository moneyJarRepository) {
        this.moneyJarRepository = moneyJarRepository;
    }

    public void getMoneyJarUserDetailList() {
        moneyJarRepository.getMoneyJarUserDetailList(moneyJarUserDetailListliveData);
    }

    public MutableLiveData<List<MoneyJarUserDetail>> getMoneyJarUserDetailListliveData() {
        return moneyJarUserDetailListliveData;
    }
}
