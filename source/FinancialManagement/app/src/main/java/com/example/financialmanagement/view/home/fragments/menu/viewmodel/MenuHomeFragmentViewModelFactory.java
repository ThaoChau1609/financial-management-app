package com.example.financialmanagement.view.home.fragments.menu.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.financialmanagement.repository.login.UserReponsitory;

public class MenuHomeFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private UserReponsitory userReponsitory;

    public MenuHomeFragmentViewModelFactory(UserReponsitory userReponsitory) {
        this.userReponsitory = userReponsitory;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MenuHomeFragmentViewModel(userReponsitory);
    }
}
