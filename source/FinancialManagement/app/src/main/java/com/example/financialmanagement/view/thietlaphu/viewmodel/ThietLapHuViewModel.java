package com.example.financialmanagement.view.thietlaphu.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;

import java.util.List;

public class ThietLapHuViewModel extends ViewModel {
    private MutableLiveData<List<MoneyJarUserDetail>> listMoneyJarLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> resultUpdateMoneyJar = new MutableLiveData<>();

    private MoneyJarRepository moneyJarRepository;

    public ThietLapHuViewModel(MoneyJarRepository moneyJarRepository) {
        this.moneyJarRepository = moneyJarRepository;
    }

    public void updateMoneyJarUserDetail(List<MoneyJarUserDetail> moneyJarUserDetails) {
        this.moneyJarRepository.updateMoneyJarUserDetailList(moneyJarUserDetails, resultUpdateMoneyJar);
    }

    public void getListMoneyJar() {
        this.moneyJarRepository.getMoneyJarUserDetailList(listMoneyJarLiveData);
    }

    public MutableLiveData<List<MoneyJarUserDetail>> getListMoneyJarLiveData() {
        return listMoneyJarLiveData;
    }

    public MutableLiveData<Boolean> getResultUpdateMoneyJar() {
        return resultUpdateMoneyJar;
    }

    public void clearLiveData() {
        this.listMoneyJarLiveData.setValue(null);
    }
}
