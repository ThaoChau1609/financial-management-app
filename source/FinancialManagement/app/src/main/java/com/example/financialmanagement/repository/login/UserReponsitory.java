package com.example.financialmanagement.repository.login;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.example.financialmanagement.App;
import com.example.financialmanagement.AppExecutors;
import com.example.financialmanagement.entities.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

public class UserReponsitory {
    private AppExecutors appExecutors;
    private Context context;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabaseUser;

    public UserReponsitory(AppExecutors appExecutors, Context context) {
        this.appExecutors = appExecutors;
        this.context = context;

        mAuth = FirebaseAuth.getInstance();
        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("User");
        mDatabaseUser.keepSynced(true);
    }

    public void login(final String userName, final String password, final MutableLiveData<Boolean> resultLoginLiveData) {
        mAuth.signInWithEmailAndPassword(userName, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // logic Success
                    String userId = mAuth.getCurrentUser().getUid();
                    User user = new User(userId,"", "", userName);

                    mDatabaseUser.child(userId).setValue(user, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            resultLoginLiveData.postValue(true);
                        }
                    });

                } else {
                    resultLoginLiveData.postValue(false);
                }
            }
        });
    }

    public void checkLoginStatus(final MutableLiveData<Boolean> loginStatus) {
        mAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    // authen user that bai
                    loginStatus.postValue(false);
                } else {
                    // authen user thanh cong
                    loginStatus.postValue(true);
                }
            }
        });
    }
    
    public void getUserInformation(final MutableLiveData<User> userMutableLiveData) {
        appExecutors.networkIO().execute(new Runnable() {
            @Override
            public void run() {
                final Gson gson = App.self().getGSon();
                String userUid = mAuth.getCurrentUser().getUid();
                DatabaseReference user = mDatabaseUser.child(userUid);
                user.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //User user1 = (User) dataSnapshot.getValue();
                        String jsonUser = gson.toJson(dataSnapshot.getValue());
                        User user1 = gson.fromJson(jsonUser, User.class);
                        userMutableLiveData.postValue(user1);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        userMutableLiveData.postValue(null);
                    }
                });
            }
        });
    }
}
