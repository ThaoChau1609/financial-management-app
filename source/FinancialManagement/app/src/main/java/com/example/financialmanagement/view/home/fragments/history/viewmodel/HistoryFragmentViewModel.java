package com.example.financialmanagement.view.home.fragments.history.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class HistoryFragmentViewModel extends ViewModel {

    private MoneyJarRepository moneyJarRepository;
    private static final String DATE_FORMAT = "MM/yyyy";
    private MutableLiveData<String> dateLiveData = new MutableLiveData<>();

    public HistoryFragmentViewModel(MoneyJarRepository moneyJarRepository) {
        this.moneyJarRepository = moneyJarRepository;
    }
    public void saveDateLiveData(String date) {
        dateLiveData.setValue(date);
    }
    public MutableLiveData<List<MoneyJarUserDetail>> getListMoneyJarUserDetails(){
        MutableLiveData<List<MoneyJarUserDetail>> listMoneyJarLiveData = new MutableLiveData<>();
        this.moneyJarRepository.getMoneyJarUserDetailList(listMoneyJarLiveData);
        return listMoneyJarLiveData;
    }
    public MutableLiveData<String> getDateLiveData() {
        return dateLiveData;
    }
    public void saveNewExchange() {

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        Date date = null;
        try {
            date = dateFormat.parse(dateLiveData.getValue());
        } catch (ParseException e) {
            Log.e("HTC", "saveNewExchange: ", e);
        }
    }

}
