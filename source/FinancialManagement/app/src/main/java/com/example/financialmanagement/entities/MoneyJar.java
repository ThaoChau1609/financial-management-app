package com.example.financialmanagement.entities;

import java.io.Serializable;

public class MoneyJar implements Serializable {
    private int idJar;
    private String description;
    private String image;
    private String name;

    public MoneyJar(int idJar, String description, String image, String name) {
        this.idJar = idJar;
        this.description = description;
        this.image = image;
        this.name = name;
    }

    public int getIdJar() {
        return idJar;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public void setIdJar(int idJar) {
        this.idJar = idJar;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "MoneyJar{" +
                "idJar=" + idJar +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
