package com.example.financialmanagement.entities;

import java.io.Serializable;

public enum ExchangeType implements Serializable {
    CHITIEU, THUNHAP
}
