package com.example.financialmanagement.view.exchange.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;

public class ListJarActivityViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private MoneyJarRepository moneyJarRepository;

    public ListJarActivityViewModelFactory(MoneyJarRepository moneyJarRepository) {
        this.moneyJarRepository = moneyJarRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ListJarActivityViewModel(moneyJarRepository);
    }
}
