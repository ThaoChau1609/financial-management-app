package com.example.financialmanagement.view.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.financialmanagement.R;
import com.example.financialmanagement.entities.User;
import com.example.financialmanagement.view.ViewModelProviderUtils;
import com.example.financialmanagement.view.home.MainActivity;
import com.example.financialmanagement.view.login.viewmodel.LoginViewModel;
import com.example.financialmanagement.view.login.viewmodel.LoginViewModelFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edtUsername;
    private EditText edtPassword;
    private ImageButton btnLogin;
    private ProgressBar progressBarLoading;

    private LoginViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        addControls();
        addEvents();

        LoginViewModelFactory factory = ViewModelProviderUtils.provideLoginViewModelFactory(LoginActivity.this);
        viewModel = ViewModelProviders.of(LoginActivity.this, factory).get(LoginViewModel.class);

        // Lang nghe ket qua (Login) callback tu LiveData
        viewModel.getResultLoginLiveData().observe(LoginActivity.this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean loginResult) {
                if (loginResult) {
                    // Login thanh cong
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    // Login that bai
                    loginLoading(false);
                    Toast.makeText(LoginActivity.this, "Login Error !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addEvents() {
        // Set Onclick cho button login
        btnLogin.setOnClickListener(this);
    }

    private void addControls() {
        edtUsername = findViewById(R.id.login_activity_edt_username_login);
        edtPassword = findViewById(R.id.login_activity_edt_password_login);
        btnLogin = findViewById(R.id.login_activity_btn_login);
        progressBarLoading = findViewById(R.id.login_activity_progressBar_loading);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnLogin)) {
            // show loading
            loginLoading(true);

            if (!TextUtils.isEmpty(edtUsername.getText().toString())
                    && !TextUtils.isEmpty(edtPassword.getText().toString())) {
                // goi function login cua LoginViewModel, neu co ket qua tra ve cho LiveData va callback len Activity
                viewModel.login(edtUsername.getText().toString(), edtPassword.getText().toString());

            } else {
                loginLoading(false);

                Toast.makeText(this, "UserName, password khong dc rong !", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void loginLoading(boolean isLoading) {
        if (isLoading) {
            progressBarLoading.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.GONE);
        } else {
            progressBarLoading.setVisibility(View.GONE);
            btnLogin.setVisibility(View.VISIBLE);
        }
    }
}
