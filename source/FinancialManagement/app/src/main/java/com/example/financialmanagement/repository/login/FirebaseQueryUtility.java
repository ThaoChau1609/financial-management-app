package com.example.financialmanagement.repository.login;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FirebaseQueryUtility {
    public static FirebaseUser getCurrentUser(){
        //get current seeder in database
        FirebaseUser currentFBaseUser = FirebaseAuth.getInstance().getCurrentUser();
        return currentFBaseUser;
    }

    public static DatabaseReference getCurrentSeederDBRef(){
        FirebaseUser currentFBaseUser = FirebaseQueryUtility.getCurrentUser();
        DatabaseReference seedersDBRef = FirebaseDatabase.getInstance().getReference().child("Seeder");
        DatabaseReference currentSeederDBRef = null;
        currentSeederDBRef = seedersDBRef.child(currentFBaseUser.getUid());
        return currentSeederDBRef;
    }
}
