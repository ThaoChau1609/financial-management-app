package com.example.financialmanagement.view.home.fragments.menu.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.financialmanagement.entities.User;
import com.example.financialmanagement.repository.login.UserReponsitory;

public class MenuHomeFragmentViewModel extends ViewModel {

    private MutableLiveData<User> userInfomationLiveData = new MutableLiveData<>();

    private UserReponsitory userReponsitory;

    public MenuHomeFragmentViewModel(UserReponsitory userReponsitory) {
        this.userReponsitory = userReponsitory;
    }

    public void getUserInformation() {
        this.userReponsitory.getUserInformation(userInfomationLiveData);
    }

    public MutableLiveData<User> getUserInfomationLiveData() {
        return userInfomationLiveData;
    }
}
