package com.example.financialmanagement.view.home.fragments.home.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;
import com.example.financialmanagement.view.home.fragments.home.HomeFragment;

import java.util.List;

public class HomeFragmentViewModel extends ViewModel {

    private MoneyJarRepository moneyJarRepository;

    public HomeFragmentViewModel(MoneyJarRepository moneyJarRepository) {
        this.moneyJarRepository = moneyJarRepository;
    }
    public MutableLiveData<List<MoneyJarUserDetail>> getListMoneyJarUserDetails(){
        MutableLiveData<List<MoneyJarUserDetail>> listMoneyJarLiveData = new MutableLiveData<>();
        this.moneyJarRepository.getMoneyJarUserDetailList(listMoneyJarLiveData);
        return listMoneyJarLiveData;
    }

}
