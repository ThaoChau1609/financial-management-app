package com.example.financialmanagement.view.home.fragments.history;


import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.financialmanagement.R;
import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.view.ViewModelProviderUtils;
import com.example.financialmanagement.view.home.fragments.history.viewmodel.HistoryFragmentViewModel;
import com.example.financialmanagement.view.home.fragments.history.viewmodel.HistoryFragmentViewModelFactory;
import com.example.financialmanagement.view.home.fragments.home.viewmodel.HomeFragmentViewModel;
import com.example.financialmanagement.view.home.fragments.home.viewmodel.HomeFragmentViewModelFactory;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment implements View.OnClickListener{

    private HistoryFragmentViewModel historyFragmentViewModel;
    private RelativeLayout itemDatePickerDialog;
    private List<MoneyJarUserDetail> moneyJarUserDetailList = new ArrayList<>();
    private List<MoneyJarUserDetail> moneyJarUserDetailExchangeList = new ArrayList<>();
    private LinearLayout linearLayoutHistory;
    private TextView tvDate;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HistoryFragmentViewModelFactory historyFragmentViewModelFactory = ViewModelProviderUtils.provideHistoryFragmentViewModelFactory(getContext());
        historyFragmentViewModel = ViewModelProviders.of(this, historyFragmentViewModelFactory).get(HistoryFragmentViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addControls(view);
        addEvents();
        historyFragmentViewModel.getListMoneyJarUserDetails().observe(this, new Observer<List<MoneyJarUserDetail>>() {
            @Override
            public void onChanged(List<MoneyJarUserDetail> moneyJarUserDetails) {
                if (moneyJarUserDetails != null) {
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    moneyJarUserDetailList.clear();
                    linearLayoutHistory.removeAllViews();
                    for (MoneyJarUserDetail moneyJar : moneyJarUserDetails) {
                        moneyJarUserDetailList.add(moneyJar);
                        View view = createItemMoneyJar(moneyJar);
                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                        linearLayoutHistory.addView(view, params);
                    }
                }
            }
        });
    }

    private void addEvents() {
        itemDatePickerDialog.setOnClickListener(this);
    }

    public void addControls(View view){
        itemDatePickerDialog = view.findViewById(R.id.relativeLayout_datePicker);
        tvDate = view.findViewById(R.id.tv_date);
        linearLayoutHistory = view.findViewById(R.id.linearLayout_historyFragment);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(itemDatePickerDialog)){
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    String date = dayOfMonth + "/" + month + "/" + year;
                    tvDate.setText(date);
                    historyFragmentViewModel.saveDateLiveData(date);
                }
            }, year, month, day);

            datePickerDialog.show();
        }
    }
    private View createItemMoneyJar(MoneyJarUserDetail moneyJar) {
        Log.i("HTC", "createItemMoneyJar: " + moneyJar);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_item_historyfragment, null, false);

        TextView tvJarExchange = view.findViewById(R.id.tv_jarExchange);
        TextView tvBudgetJar = view.findViewById(R.id.tv_budgetJar);
        TextView tvTypeExchange = view.findViewById(R.id.tv_typeExchange);
        TextView tvDateExchange = view.findViewById(R.id.tv_dateExchange);
        // set Data
        //tvJarName.setText(moneyJar.getMoneyJar().getName());
        //tvBudget.setText(String.valueOf(moneyJar.getBudget()));
        return view;
    }
}
