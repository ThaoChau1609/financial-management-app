package com.example.financialmanagement.view.exchange;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.financialmanagement.R;
import com.example.financialmanagement.entities.Exchange;
import com.example.financialmanagement.entities.ExchangeType;
import com.example.financialmanagement.entities.MoneyJar;
import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.view.ViewModelProviderUtils;
import com.example.financialmanagement.view.exchange.viewmodel.ExchangeActivityViewModel;
import com.example.financialmanagement.view.exchange.viewmodel.ExchangeActivityViewModelFactory;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ExchangeActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    public static final int REQUEST_CODE_ITEM_JAR = 111;

    private RadioGroup radioExchangeType;

    private EditText edtSoTien;

    private TextView tvDate;

    private RelativeLayout itemAddJar;
    private TextView tvJarName;

    private RelativeLayout itemSelectDate;

    private EditText edtDescription;

    private Button btnCancel;
    private Button btnSave;

    private ExchangeActivityViewModel viewModel;
    private List<MoneyJarUserDetail> moneyJarUserDetailList = new ArrayList<>();


    @Override
    public void onBackPressed() {
        viewModel.clearAllLiveData();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange);
        addControl();
        addevents();
        ExchangeActivityViewModelFactory factory = ViewModelProviderUtils.provideExchangeActivityViewModelFactory(this);
        viewModel = ViewModelProviders.of(this, factory).get(ExchangeActivityViewModel.class);

        // Init default
        viewModel.saveExchangeTypeLiveData(ExchangeType.CHITIEU);

        onLiveData();

        viewModel.getResultNewExchange().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean result) {
                if (result != null) {
                    if (result) {
                        Toast.makeText(ExchangeActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(ExchangeActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void addControl() {
        radioExchangeType = findViewById(R.id.radiogroup_type_exchange);
        edtSoTien = findViewById(R.id.edt_exchange_amount);
        tvDate = findViewById(R.id.tv_date_selected);
        this.itemAddJar = findViewById(R.id.activity_exchange_addjar);
        tvJarName = findViewById(R.id.tv_jar_name);
        itemSelectDate = findViewById(R.id.activity_exchange_canlendar);
        edtDescription = findViewById(R.id.edt_exchange_description);
        btnCancel = findViewById(R.id.exchange_btn_cancel);
        btnSave = findViewById(R.id.exchange_btn_save);
    }

    private void addevents() {
        itemAddJar.setOnClickListener(this);
        itemSelectDate.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        radioExchangeType.setOnCheckedChangeListener(this);
        // save so tien khi nhap
        edtSoTien.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.saveSoTien(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // save description khi nhap
        edtDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                viewModel.saveDescription(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void onLiveData() {
        // on change MoneyJarUserDetailLiveData
        viewModel.getMoneyJarUserDetailLiveData().observe(this, new Observer<MoneyJarUserDetail>() {
            @Override
            public void onChanged(MoneyJarUserDetail moneyJarUserDetail) {
                Log.i("HTC", "getMoneyJarUserDetailLiveData: " + moneyJarUserDetail);
                tvJarName.setText(moneyJarUserDetail.getMoneyJar().getName());
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.equals(itemAddJar)) {
            Intent intent = new Intent(ExchangeActivity.this, ListJarActivity.class);
            startActivityForResult(intent, REQUEST_CODE_ITEM_JAR);
        }
        if (v.equals(itemSelectDate)) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    String date = dayOfMonth + "/" + month + "/" + year;
                    tvDate.setText(date);
                    viewModel.saveDateLiveData(date);
                }
            }, year, month, day);

            datePickerDialog.show();
        }
        if (v.equals(btnCancel)) {
            finish();
        }
        if (v.equals(btnSave)) {
            int percent;
            Double soTienOfPercent;
            Double soTienLiveData = Double.parseDouble(edtSoTien.getText().toString());
            ExchangeType exchangeTypeAllJar = viewModel.getExchangeTypeLiveData().getValue();
            if (tvJarName.getText().toString().equalsIgnoreCase("Tất cả")) {
                Log.i("HTC1", "onClick: Ahihi Thành công bước đầu");
                if (moneyJarUserDetailList != null && viewModel.getSoTienLiveData() != null
                        && viewModel.getDateLiveData() != null && exchangeTypeAllJar == ExchangeType.THUNHAP) {
                    for (MoneyJarUserDetail moneyJarUserDetail : moneyJarUserDetailList) {
                        percent = moneyJarUserDetail.getPercent();
                        soTienOfPercent = (soTienLiveData * percent) / 100;
                        Log.i("HTC1", "onClick: " + soTienOfPercent);
                        viewModel.saveSoTien(soTienOfPercent.toString());
                        viewModel.saveMoneyJarUserDetailAllJar(moneyJarUserDetail);
                        viewModel.saveNewExchangeAllJar();
                        Log.i("HTC1", "onClick: " + viewModel.getMoneyJarUserDetailLiveDataAllJar());
                    }
                } else {
                    Toast.makeText(this, "Chỉ chọn hủ cần chi tiêu", Toast.LENGTH_SHORT).show();
                }
            }
            if (viewModel.getMoneyJarUserDetailLiveData().getValue() != null
                    && viewModel.getSoTienLiveData().getValue() != null
                    && viewModel.getExchangeTypeLiveData().getValue() != null) {
                ExchangeType exchangeType = viewModel.getExchangeTypeLiveData().getValue();
                String sotien = viewModel.getSoTienLiveData().getValue();
                MoneyJarUserDetail moneyJarUserDetail = viewModel.getMoneyJarUserDetailLiveData().getValue();

                if (exchangeType == ExchangeType.CHITIEU) {
                    if (Double.parseDouble(sotien) > moneyJarUserDetail.getBudget()) {
                        Toast.makeText(this, "Số tiền trong hũ không đủ chi tiêu !", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                viewModel.saveNewExchange();
            } /*else {
                Toast.makeText(this, "Chưa nhập thông tin đầy đủ !", Toast.LENGTH_SHORT).show();
            }*/
            /*if (viewModel.getMoneyJarUserDetailLiveData().getValue() != null
                    && viewModel.getSoTienLiveData().getValue() != null
                    && viewModel.getExchangeTypeLiveData().getValue() != null) {
                ExchangeType exchangeType = viewModel.getExchangeTypeLiveData().getValue();
                String sotien = viewModel.getSoTienLiveData().getValue();
                MoneyJarUserDetail moneyJarUserDetail = viewModel.getMoneyJarUserDetailLiveData().getValue();

                if (exchangeType == ExchangeType.CHITIEU) {
                    if (Double.parseDouble(sotien) > moneyJarUserDetail.getBudget()) {
                        Toast.makeText(this, "Số tiền trong hũ không đủ chi tiêu !", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                viewModel.saveNewExchange();*/
        } /*else {
                if (edtSoTien.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(this, "Chưa nhập số tiền !", Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(this, "Chưa nhập thông tin đầy đủ !", Toast.LENGTH_SHORT).show();
            }*/
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radio_type_chitieu:
                viewModel.saveExchangeTypeLiveData(ExchangeType.CHITIEU);
                break;
            case R.id.radio_type_thunhap:
                viewModel.saveExchangeTypeLiveData(ExchangeType.THUNHAP);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_ITEM_JAR:
                    if (data.getStringExtra(ListJarActivity.KEY_MONEY_JARALL_SELECTED) != null) {
                        tvJarName.setText(data.getStringExtra(ListJarActivity.KEY_MONEY_JARALL_SELECTED));
                        viewModel.getListMoneyJarUserDetails().observe(this, new Observer<List<MoneyJarUserDetail>>() {
                            @Override
                            public void onChanged(List<MoneyJarUserDetail> moneyJarUserDetails) {
                                Log.i("HTC1", "getMoneyJarUserDetailLiveData: " + moneyJarUserDetails);
                                for (MoneyJarUserDetail moneyJar : moneyJarUserDetails) {
                                    moneyJarUserDetailList.add(moneyJar);
                                }
                            }
                        });
                    }

                    if (data.getSerializableExtra(ListJarActivity.KEY_MONEY_JAR_SELECTED) != null) {
                        MoneyJarUserDetail moneyJarUserDetail =
                                (MoneyJarUserDetail) data.getSerializableExtra(ListJarActivity.KEY_MONEY_JAR_SELECTED);
                        viewModel.saveMoneyJarUserDetail(moneyJarUserDetail);
                    }
                    break;
            }
        }

      /*  public void newExchageForALL () {
            viewModel.getListMoneyJarUserDetails().observe(this, new Observer<List<MoneyJarUserDetail>>() {
                @Override
                public void onChanged(List<MoneyJarUserDetail> moneyJarUserDetails) {
                    if (moneyJarUserDetails != null) {
                        for (MoneyJarUserDetail moneyJar : moneyJarUserDetails) {
                            moneyJarUserDetailList.add(moneyJar);
                        }
                    }

                }
            });
        }*/
    }

    private String formatVNcurrency(String price) {
        double priceCurrency = Double.parseDouble(price);
        DecimalFormat formatter = new DecimalFormat("###,###,##0");
        return formatter.format(priceCurrency);
    }
}

