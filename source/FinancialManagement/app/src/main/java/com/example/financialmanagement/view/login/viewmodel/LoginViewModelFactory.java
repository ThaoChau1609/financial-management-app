package com.example.financialmanagement.view.login.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.financialmanagement.repository.login.UserReponsitory;

public class LoginViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private UserReponsitory userReponsitory;

    public LoginViewModelFactory(UserReponsitory userReponsitory) {
        this.userReponsitory = userReponsitory;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new LoginViewModel(userReponsitory);
    }
}
