package com.example.financialmanagement.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class Exchange implements Serializable {
    private String id;
    private int type;
    private double amount;
    private String note;
    private Date date;

    public Exchange(String id, ExchangeType type, double amount, String note, Date date) {
        this.id = id;
        this.type = type == ExchangeType.THUNHAP ? 1 : 0;
        this.amount = amount;
        this.note = note;
        this.date = date;
    }

    public Exchange(ExchangeType type, double amount, String note, Date date) {
        this.id = UUID.randomUUID().toString();
        this.type = type == ExchangeType.THUNHAP ? 1 : 0;
        this.amount = amount;
        this.note = note;
        this.date = date;
    }

    public Exchange(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exchange exchange = (Exchange) o;
        return id == exchange.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ExchangeType getExchangeTypeEnum() {
        return this.type == 1 ? ExchangeType.THUNHAP : ExchangeType.CHITIEU;
    }
    public void setExchangeTypeEnum(ExchangeType exchangeTypeEnum) {
        this.type = exchangeTypeEnum == ExchangeType.THUNHAP ? 1 : 0;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Exchange{" +
                "id=" + id +
                ", type=" + type +
                ", amount=" + amount +
                ", note='" + note + '\'' +
                ", date=" + date +
                '}';
    }
}
