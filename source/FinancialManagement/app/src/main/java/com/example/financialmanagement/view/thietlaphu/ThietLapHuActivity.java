package com.example.financialmanagement.view.thietlaphu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.financialmanagement.R;
import com.example.financialmanagement.entities.MoneyJar;
import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.view.ViewModelProviderUtils;
import com.example.financialmanagement.view.thietlaphu.viewmodel.ThietLapHuViewModel;
import com.example.financialmanagement.view.thietlaphu.viewmodel.ThietLapHuViewModelFactory;
import com.github.mikephil.charting.charts.PieChart;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.gms.flags.impl.DataUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThietLapHuActivity extends AppCompatActivity implements View.OnClickListener {

    private int[] moneyJarImageList = new int[] {
            R.drawable.ic_jar1,
            R.drawable.ic_jar2,
            R.drawable.ic_jar3,
            R.drawable.ic_jar4,
            R.drawable.ic_jar5,
            R.drawable.ic_jar6
    };

    private int currentMoneyJarIC;

    private PieChart pieChartSum;

    private FlexboxLayout flexboxLayoutJar;
    private Button btnCancel;

    private ThietLapHuViewModel viewModel;

    private List<MoneyJarUserDetail> moneyJarUserDetails = new ArrayList<>();

    @Override
    public void onBackPressed() {
        viewModel.clearLiveData();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thiet_lap_hu);

        addControls();
        addEvents();

        ThietLapHuViewModelFactory factory = ViewModelProviderUtils.provideThietLapHuViewModelFactory(this);
        viewModel = ViewModelProviders.of(this, factory).get(ThietLapHuViewModel.class);

        viewModel.getListMoneyJar();
        viewModel.getListMoneyJarLiveData().observe(this, new Observer<List<MoneyJarUserDetail>>() {
            @Override
            public void onChanged(List<MoneyJarUserDetail> moneyJarUserDetailList) {
                if (moneyJarUserDetails != null) {
                    currentMoneyJarIC = 0;
                    moneyJarUserDetails.clear();
                    flexboxLayoutJar.removeAllViews();
                    for (MoneyJarUserDetail moneyJar : moneyJarUserDetailList) {
                        View view = createItemMoneyJar(moneyJar);
                        flexboxLayoutJar.addView(view);
                        moneyJarUserDetails.add(moneyJar);
                    }
                }
            }
        });
    }

    private View createItemMoneyJar(final MoneyJarUserDetail moneyJar) {
        View view = getLayoutInflater().inflate(R.layout.view_item_moneyjar, null);

        TextView tvJarName = view.findViewById(R.id.view_item_moneyjar_layout_tv_jarName);
        ImageView imgJar = view.findViewById(R.id.view_item_moneyjar_layout_img_jar);
        final EditText edtPercenJar = view.findViewById(R.id.view_item_moneyjar_layout_edt_jar);
        ImageButton btnDivide = view.findViewById(R.id.view_item_moneyjar_layout_btn_divide_jar1);
        ImageButton btnSum = view.findViewById(R.id.view_item_moneyjar_layout_btn_sum_jar);

        btnDivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String percen = edtPercenJar.getText().toString();
                try {
                    int newPercenNumber = Integer.parseInt(percen);
                    newPercenNumber = (newPercenNumber - 5) < 0 ? 0 : (newPercenNumber - 5);
                    setValueJarList(edtPercenJar, newPercenNumber, moneyJar);
                } catch (NumberFormatException e) {
                    Toast.makeText(ThietLapHuActivity.this, "Percent can't parse !", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String percen = edtPercenJar.getText().toString();
                try {
                    int percenNumber = Integer.parseInt(percen);
                    percenNumber = (percenNumber + 5) > 100 ? 100 : (percenNumber + 5);
                    setValueJarList(edtPercenJar, percenNumber, moneyJar);
                } catch (NumberFormatException e) {
                    Toast.makeText(ThietLapHuActivity.this, "Percent can't parse !", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // set Data
        if (moneyJar.getMoneyJar() != null) {
            tvJarName.setText(moneyJar.getMoneyJar().getName());

            if (URLUtil.isValidUrl(moneyJar.getMoneyJar().getImage())) {
                Glide.with(this).load(moneyJar.getMoneyJar().getImage()).into(imgJar);
            } else {
                if (this.currentMoneyJarIC < moneyJarImageList.length) {
                    imgJar.setImageResource(moneyJarImageList[this.currentMoneyJarIC]);
                    this.currentMoneyJarIC++;
                }
            }
        }

        edtPercenJar.setText(String.valueOf(moneyJar.getPercent()));

        return view;
    }

    private void setValueJarList(EditText editText, int newPercenNumber, MoneyJarUserDetail moneyJarUserDetail) {
        int sum = 0;
        for (MoneyJarUserDetail moneyJar : this.moneyJarUserDetails) {
            if (moneyJarUserDetail.getId().equals(moneyJar.getId())) {
                sum += newPercenNumber;
            } else {
                sum += moneyJar.getPercent();
            }
        }
        if (sum > 100) {
            Toast.makeText(this, "Total > 100 !", Toast.LENGTH_SHORT).show();
            editText.setText(String.valueOf(moneyJarUserDetail.getPercent()));
        } else {
            moneyJarUserDetail.setPercent(newPercenNumber);
            editText.setText(String.valueOf(moneyJarUserDetail.getPercent()));
        }

        moneyJarUserDetails.set(moneyJarUserDetails.indexOf(moneyJarUserDetail), moneyJarUserDetail);

        // update to Firebase
        viewModel.updateMoneyJarUserDetail(moneyJarUserDetails);
        viewModel.getResultUpdateMoneyJar().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean result) {
                if (result != null) {
                    if (result) {
                        Toast.makeText(ThietLapHuActivity.this, "Update success", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ThietLapHuActivity.this, "Can't update", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void addEvents() {
        btnCancel.setOnClickListener(this);
    }

    private void addControls() {
        pieChartSum = findViewById(R.id.thietlap_hu_chart);
        flexboxLayoutJar = findViewById(R.id.thietlap_hu_layout_jar);
        btnCancel = findViewById(R.id.thietlap_hu_btn_cancel);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnCancel)) {
           checkPercentJar();
        }
    }
    public void checkPercentJar(){
        int totalPercent = 0;
        for(MoneyJarUserDetail moneyJarUser : moneyJarUserDetails){
            totalPercent = totalPercent + moneyJarUser.getPercent();
        }
        if (totalPercent < 100){
            Toast.makeText(ThietLapHuActivity.this, "Tổng phần trăm các hủ phải bằng 100%", Toast.LENGTH_SHORT).show();
        }else{
            finish();
        }
    }
}
