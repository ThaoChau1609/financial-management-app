package com.example.financialmanagement.view.exchange.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.financialmanagement.entities.Exchange;
import com.example.financialmanagement.entities.ExchangeType;
import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ExchangeActivityViewModel extends ViewModel {

    private static final String DATE_FORMAT = "dd/MM/yyyy";

    private MutableLiveData<ExchangeType> exchangeTypeLiveData = new MutableLiveData<>();
    private MutableLiveData<String> dateLiveData = new MutableLiveData<>();
    private MutableLiveData<String> soTienLiveData = new MutableLiveData<>();
    private MutableLiveData<String> descriptionLiveData = new MutableLiveData<>();
    private MutableLiveData<MoneyJarUserDetail> moneyJarUserDetailLiveData = new MutableLiveData<>();
    private MutableLiveData<MoneyJarUserDetail> moneyJarUserDetailLiveDataAllJar = new MutableLiveData<>();

    private MutableLiveData<Boolean> resultNewExchange = new MutableLiveData<>();

    private MoneyJarRepository moneyJarRepository;

    public ExchangeActivityViewModel(MoneyJarRepository moneyJarRepository) {
        this.moneyJarRepository = moneyJarRepository;
    }

    public void saveExchangeTypeLiveData(ExchangeType exchangeType) {
        exchangeTypeLiveData.setValue(exchangeType);
    }

    public MutableLiveData<ExchangeType> getExchangeTypeLiveData() {
        return exchangeTypeLiveData;
    }

    public void saveDateLiveData(String date) {
        dateLiveData.setValue(date);
    }

    public MutableLiveData<String> getDateLiveData() {
        return dateLiveData;
    }

    public void saveSoTien(String sotien) {
        soTienLiveData.setValue(sotien);
    }

    public MutableLiveData<String> getSoTienLiveData() {
        return soTienLiveData;
    }

    public void saveDescription(String description) {
        descriptionLiveData.setValue(description);
    }

    public MutableLiveData<String> getDescriptionLiveData() {
        return descriptionLiveData;
    }

    public void saveMoneyJarUserDetail(MoneyJarUserDetail moneyJarUserDetail) {
        moneyJarUserDetailLiveData.setValue(moneyJarUserDetail);
    }
    public void saveMoneyJarUserDetailAllJar(MoneyJarUserDetail moneyJarUserDetail) {
        moneyJarUserDetailLiveDataAllJar.setValue(moneyJarUserDetail);
    }

    public MutableLiveData<MoneyJarUserDetail> getMoneyJarUserDetailLiveData() {
        return moneyJarUserDetailLiveData;
    }
    public MutableLiveData<MoneyJarUserDetail> getMoneyJarUserDetailLiveDataAllJar() {
        return moneyJarUserDetailLiveDataAllJar;
    }

    public void saveNewExchange() {
        Log.i("HTC", "exchangeTypeLiveData: " + exchangeTypeLiveData.getValue());

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        Date date = null;
        try {
            date = dateFormat.parse(dateLiveData.getValue());
        } catch (ParseException e) {
            Log.e("HTC", "saveNewExchange: ", e);
        }

        Exchange exchange = new Exchange(
                exchangeTypeLiveData.getValue(),
                Double.parseDouble(soTienLiveData.getValue()),
                descriptionLiveData.getValue(),
                date);

        moneyJarRepository.addNewExchange(exchange, moneyJarUserDetailLiveData.getValue(), resultNewExchange);
    }

    public MutableLiveData<Boolean> getResultNewExchange() {
        return resultNewExchange;
    }

    public void clearAllLiveData() {
        this.exchangeTypeLiveData.setValue(null);
        this.dateLiveData.setValue(null);
        this.soTienLiveData.setValue(null);
        this.descriptionLiveData.setValue(null);
        this.moneyJarUserDetailLiveData.setValue(null);
    }
    public MutableLiveData<List<MoneyJarUserDetail>> getListMoneyJarUserDetails(){
        MutableLiveData<List<MoneyJarUserDetail>> listMoneyJarLiveData = new MutableLiveData<>();
        this.moneyJarRepository.getMoneyJarUserDetailList(listMoneyJarLiveData);
        return listMoneyJarLiveData;
    }

    public void saveNewExchangeAllJar() {
        Log.i("HTC", "exchangeTypeLiveData: " + exchangeTypeLiveData.getValue());

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        Date date = null;
        try {
            date = dateFormat.parse(dateLiveData.getValue());
        } catch (ParseException e) {
            Log.e("HTC", "saveNewExchange: ", e);
        }

        Exchange exchange = new Exchange(
                exchangeTypeLiveData.getValue(),
                Double.parseDouble(soTienLiveData.getValue()),
                descriptionLiveData.getValue(),
                date);

        moneyJarRepository.addNewExchange(exchange, moneyJarUserDetailLiveDataAllJar.getValue(), resultNewExchange);
    }
}
