package com.example.financialmanagement.view.home.fragments.history.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.financialmanagement.repository.moneyjar.MoneyJarRepository;
import com.example.financialmanagement.view.home.fragments.home.viewmodel.HomeFragmentViewModel;

public class HistoryFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private MoneyJarRepository moneyJarRepository;

    public HistoryFragmentViewModelFactory(MoneyJarRepository moneyJarRepository) {
        this.moneyJarRepository = moneyJarRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new HistoryFragmentViewModel(moneyJarRepository);
    }

}
