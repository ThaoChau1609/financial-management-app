package com.example.financialmanagement.repository;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.financialmanagement.App;
import com.google.gson.Gson;

public class LocalSharePreferences {
    private static final String PREFS_NAME = "local_store";
    private static LocalSharePreferences mInstance;
    private SharedPreferences mSharedPreferences;
    private Gson gson;

    private LocalSharePreferences() {
        mSharedPreferences = App.self().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        this.gson = App.self().getGSon();
    }

    public static LocalSharePreferences getInstance() {
        if (mInstance == null) {
            mInstance = new LocalSharePreferences();
        }
        return mInstance;
    }

    public <T> void put(String key, T data) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        if (data instanceof String) {
            editor.putString(key, (String) data);
        } else if (data instanceof Boolean) {
            editor.putBoolean(key, (Boolean) data);
        } else if (data instanceof Float) {
            editor.putFloat(key, (Float) data);
        } else if (data instanceof Integer) {
            editor.putInt(key, (Integer) data);
        } else if (data instanceof Long) {
            editor.putLong(key, (Long) data);
        } else {
            editor.putString(key, gson.toJson(data));
        }
        editor.apply();
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> anonymousClass, T defaultValue) {
        if (anonymousClass == String.class) {
            return (T) mSharedPreferences.getString(key, (String) defaultValue);
        } else if (anonymousClass == Boolean.class) {
            return (T) Boolean.valueOf(mSharedPreferences.getBoolean(key, (Boolean) defaultValue));
        } else if (anonymousClass == Float.class) {
            return (T) Float.valueOf(mSharedPreferences.getFloat(key, (Float) defaultValue));
        } else if (anonymousClass == Integer.class) {
            return (T) Integer.valueOf(mSharedPreferences.getInt(key, (Integer) defaultValue));
        } else if (anonymousClass == Long.class) {
            return (T) Long.valueOf(mSharedPreferences.getLong(key, (Long) defaultValue));
        } else {
            return (T) gson.fromJson(mSharedPreferences.getString(key, gson.toJson(defaultValue)), anonymousClass);
        }
    }


}
