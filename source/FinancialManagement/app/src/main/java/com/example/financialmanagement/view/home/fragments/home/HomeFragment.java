package com.example.financialmanagement.view.home.fragments.home;


import android.content.Context;
import android.icu.util.LocaleData;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.financialmanagement.R;
import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.view.ViewModelProviderUtils;
import com.example.financialmanagement.view.home.fragments.home.viewmodel.HomeFragmentViewModel;
import com.example.financialmanagement.view.home.fragments.home.viewmodel.HomeFragmentViewModelFactory;
import com.example.financialmanagement.view.thietlaphu.ThietLapHuActivity;
import com.facebook.login.widget.LoginButton;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {
    private LinearLayout linearLayoutJarDashboard;
    private TextView tv_totalBudget_dashboard;
    private TextView tv_hanmuchitieu;
    private HomeFragmentViewModel homeFragmentViewModel;
    private List<MoneyJarUserDetail> moneyJarUserDetailList = new ArrayList<>();
    private int currentMoneyJarIC;
    private int[] moneyJarImageList = new int[] {
            R.drawable.ic_jar1,
            R.drawable.ic_jar2,
            R.drawable.ic_jar3,
            R.drawable.ic_jar4,
            R.drawable.ic_jar5,
            R.drawable.ic_jar6
    };

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeFragmentViewModelFactory homeFragmentViewModelFactory = ViewModelProviderUtils.provideHomeFragmentViewModelFactory(getContext());
        homeFragmentViewModel = ViewModelProviders.of(this, homeFragmentViewModelFactory).get(HomeFragmentViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_home, container, false);

    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addControls(view);
        addEvents();
        homeFragmentViewModel.getListMoneyJarUserDetails().observe(this, new Observer<List<MoneyJarUserDetail>>() {
            @Override
            public void onChanged(List<MoneyJarUserDetail> moneyJarUserDetails) {
                if (moneyJarUserDetails != null) {
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    currentMoneyJarIC = 0;
                    moneyJarUserDetailList.clear();
                    linearLayoutJarDashboard.removeAllViews();
                    for (MoneyJarUserDetail moneyJar : moneyJarUserDetails) {
                        moneyJarUserDetailList.add(moneyJar);
                        View view = createItemMoneyJar(moneyJar);
                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                        linearLayoutJarDashboard.addView(view, params);
                    }
                    calculatorHanMucchiTieu();
                    tv_totalBudget_dashboard.setText(formatVNcurrency(setValueTotalBudget()));

                }
            }
        });
        //setValueBudgetForJar(tv_budget_thietyeu);
        //homeFragmentViewModel.getListMoneyJar();

    }
    private void addControls(View view) {
        linearLayoutJarDashboard = view.findViewById(R.id.linearLayout_dashboard);
        tv_totalBudget_dashboard = view.findViewById(R.id.tv_budget_total);
        tv_hanmuchitieu = view.findViewById(R.id.tv_hanmuchitieu);
    }
    private void addEvents(){
        /*fragment_home_thietyeu.setOnClickListener(this);
        fragment_home_giaoduc.setOnClickListener(this);
        fragment_home_tietkiem.setOnClickListener(this);
        fragment_home_huongthu.setOnClickListener(this);
        fragment_home_dautu.setOnClickListener(this);
        fragment_home_chiase.setOnClickListener(this);*/
    }
    private View createItemMoneyJar(MoneyJarUserDetail moneyJar) {
        Log.i("HTC", "createItemMoneyJar: " + moneyJar);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_itemjar_dashboard, null, false);

        TextView tvJarName = view.findViewById(R.id.view_itemjar_dashboard_tvJarName);
        ImageView imgJar = view.findViewById(R.id.view_itemjar_dashboard_imgJar);
        TextView tvBudget = view.findViewById(R.id.view_itemjar_dashboard_tvBudget);
        // set Data
        tvJarName.setText(moneyJar.getMoneyJar().getName());
        //tvBudget.setText(String.valueOf(moneyJar.getBudget()));
        tvBudget.setText(formatVNcurrency(String.valueOf(moneyJar.getBudget())));
        if (URLUtil.isValidUrl(moneyJar.getMoneyJar().getImage())) {
            Glide.with(this).load(moneyJar.getMoneyJar().getImage()).into(imgJar);
        } else {
            if (this.currentMoneyJarIC < moneyJarImageList.length) {
                imgJar.setImageResource(moneyJarImageList[this.currentMoneyJarIC]);
                this.currentMoneyJarIC++;
            }
        }
        return view;
    }
    private void formatCurrency(TextView textView){
        textView.setText(formatVNcurrency(textView.getText().toString()));
    }
    private String formatVNcurrency(String price){
        double priceCurrency = Double.parseDouble(price);
        DecimalFormat formatter = new DecimalFormat("###,###,##0");
        return formatter.format(priceCurrency);
    }
    private String setValueTotalBudget(){
        double sum = 0;
        for (MoneyJarUserDetail moneyJar : moneyJarUserDetailList){
             sum = sum + moneyJar.getBudget();
        }
        return String.valueOf(sum);
    }
    @Override
    public void onClick(View v) {

    }

    public void calculatorHanMucchiTieu(){
        Date today = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DATE, -1);
        Date lastDayOfMonth = calendar.getTime();
        DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        long diff = lastDayOfMonth.getTime()- today.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        for (MoneyJarUserDetail moneyJar : moneyJarUserDetailList){
            if (moneyJar.getMoneyJar().getName().equalsIgnoreCase("Thiết yếu")){
                Log.i("HTC", "calculatorHanMucchiTieu: " + moneyJar.getBudget());
                double priceHanMucChiTieu = (moneyJar.getBudget())/diffDays;
                tv_hanmuchitieu.setText(formatVNcurrency(String.valueOf(priceHanMucChiTieu)));
            }
        }
    }

}
