package com.example.financialmanagement.repository.moneyjar;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.example.financialmanagement.App;
import com.example.financialmanagement.AppExecutors;
import com.example.financialmanagement.comons.ConvertUtils;
import com.example.financialmanagement.entities.Exchange;
import com.example.financialmanagement.entities.ExchangeType;
import com.example.financialmanagement.entities.MoneyJar;
import com.example.financialmanagement.entities.MoneyJarUserDetail;
import com.example.financialmanagement.entities.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MoneyJarRepository {

    private static final String USER_STORE = User.class.getSimpleName();
    private static final String MONEYJAR_STORE = MoneyJar.class.getSimpleName();
    private static final String MONEYJAR_USER_DETAIL = MoneyJarUserDetail.class.getSimpleName();
    public static final String EXCHANGE_SOTRE = Exchange.class.getSimpleName();

    private FirebaseUser currentUser;
    private DatabaseReference mDatabaseUser;
    private DatabaseReference mDatabaseMoneyJar;
    private AppExecutors appExecutors;
    private Context context;

    public MoneyJarRepository(AppExecutors executors, Context context) {
        this.appExecutors = executors;
        this.context = context;
        this.currentUser = FirebaseAuth.getInstance().getCurrentUser();
        this.mDatabaseUser = FirebaseDatabase.getInstance().getReference().child(USER_STORE);
        this.mDatabaseMoneyJar = FirebaseDatabase.getInstance().getReference().child(MONEYJAR_STORE);
        this.mDatabaseMoneyJar.keepSynced(true);
    }

    public void getMoneyJarUserDetailList(final MutableLiveData<List<MoneyJarUserDetail>> liveData) {
        final Gson gson = App.self().getGSon();

            if (currentUser == null) {
                liveData.postValue(new ArrayList<MoneyJarUserDetail>());
                return;
        }

        DatabaseReference mDatabaseMoneyJarUserDetail =
                this.mDatabaseUser.child(currentUser.getUid()).child(MONEYJAR_USER_DETAIL);
        mDatabaseMoneyJarUserDetail.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                    List<MoneyJarUserDetail> moneyJarUserDetails = new ArrayList<>();
                    while (iterator.hasNext()) {
                        DataSnapshot item = iterator.next();
                        String jsonMoneyJar = gson.toJson(item.getValue());
                        MoneyJarUserDetail jarUserDetail = gson.fromJson(jsonMoneyJar, MoneyJarUserDetail.class);
                        moneyJarUserDetails.add(jarUserDetail);
                    }

                    liveData.postValue(moneyJarUserDetails);
                } else {
                    getListMoneyJar(liveData);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                liveData.postValue(new ArrayList<MoneyJarUserDetail>());
            }
        });
    }

    private void getListMoneyJar(final MutableLiveData<List<MoneyJarUserDetail>> listMoneyJarLiveData){
        final Gson gson = App.self().getGSon();
        mDatabaseMoneyJar.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<MoneyJarUserDetail> moneyJarList = new ArrayList<>();
                Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                while (iterator.hasNext()) {
                    DataSnapshot item = iterator.next();
                    String jsonMoneyJar = gson.toJson(item.getValue());
                    MoneyJar moneyJar = gson.fromJson(jsonMoneyJar, MoneyJar.class);
                    MoneyJarUserDetail moneyJarUserDetail = new MoneyJarUserDetail(moneyJar);
                    moneyJarList.add(moneyJarUserDetail);
                }

                listMoneyJarLiveData.postValue(moneyJarList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listMoneyJarLiveData.postValue(new ArrayList<MoneyJarUserDetail>());
            }
        });
    }

    public void updateMoneyJarUserDetailList(List<MoneyJarUserDetail> moneyJarUserDetails, final MutableLiveData<Boolean> resultLiveData) {
        if (currentUser == null) {
            resultLiveData.postValue(false);
            return;
        }

        Map<String, MoneyJarUserDetail> moneyJarUserDetailMap = ConvertUtils.convertListToMapMoneyJarUserDetail(moneyJarUserDetails);
        DatabaseReference mDatabaseMoneyJarUserDetail =
                this.mDatabaseUser.child(currentUser.getUid()).child(MONEYJAR_USER_DETAIL);
        mDatabaseMoneyJarUserDetail.setValue(moneyJarUserDetailMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError == null) {
                    resultLiveData.postValue(true);
                } else {
                    resultLiveData.postValue(false);
                }
            }
        });


    }

    public void addNewExchange(Exchange exchange, final MoneyJarUserDetail moneyJarUserDetail, final MutableLiveData<Boolean> result) {
        if (currentUser == null) {
            result.postValue(false);
            return;
        }

        final double budget = calcBudget(exchange, moneyJarUserDetail);
        moneyJarUserDetail.setBudget(budget);
        moneyJarUserDetail.addNewExChange(exchange);

        DatabaseReference mDatabaseMoneyJarUserDetail =
                this.mDatabaseUser.child(currentUser.getUid()).child(MONEYJAR_USER_DETAIL);
        mDatabaseMoneyJarUserDetail.child(moneyJarUserDetail.getId()).child(EXCHANGE_SOTRE)
                .child(exchange.getId()).setValue(exchange,
                        new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError,
                                               @NonNull DatabaseReference databaseReference) {
                            if (databaseError == null) {

                                updateBudgetJar(moneyJarUserDetail.getId(), budget, result);


                            } else {
                                result.postValue(false);
                            }
                        }
        });
    }

    private void updateBudgetJar(String id, double budget, final MutableLiveData<Boolean> result) {
        DatabaseReference mDatabaseMoneyJarUserDetail =
                this.mDatabaseUser.child(currentUser.getUid()).child(MONEYJAR_USER_DETAIL);
        mDatabaseMoneyJarUserDetail.child(id).child("budget").setValue(budget, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError == null) {
                    result.postValue(true);
                } else {
                    result.postValue(false);
                }
            }
        });
    }

    private double calcBudget(Exchange exchange, MoneyJarUserDetail moneyJarUserDetail) {
        ExchangeType exchangeType = exchange.getExchangeTypeEnum();
        double sotien = exchange.getAmount();
        double tiengoc = moneyJarUserDetail.getBudget();
        if (exchangeType == ExchangeType.CHITIEU) {
            if (tiengoc - sotien <= 0) {
                return 0;
            } else {
                return tiengoc - sotien;
            }
        } else if (exchangeType == ExchangeType.THUNHAP) {
            return tiengoc + sotien;
        }
        return 0;
    }
}
