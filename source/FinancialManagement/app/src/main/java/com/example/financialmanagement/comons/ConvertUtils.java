package com.example.financialmanagement.comons;

import com.example.financialmanagement.entities.MoneyJarUserDetail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConvertUtils {

    public static Map<String, MoneyJarUserDetail> convertListToMapMoneyJarUserDetail(List<MoneyJarUserDetail> moneyJarUserDetails) {
        Map<String, MoneyJarUserDetail> map = new HashMap<>();
        for (MoneyJarUserDetail moneyJarUserDetail : moneyJarUserDetails) {
            map.put(moneyJarUserDetail.getId(), moneyJarUserDetail);
        }
        return map;
    }
}
